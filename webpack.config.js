const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
  const mode = env || 'production';

  const target = process.env.WEBPACK_TARGET || 'http://localhost:4000';
  const host = process.env.WEBPACK_HOST || 'localhost';
  const port = process.env.WEBPACK_PORT || 4001;
  
  return {
    entry: "./_src/index.js",
    output: {
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
      hot: true,
      contentBase: path.join(__dirname, '_site'),
      proxy: {
        path: ['!**/*.js'],
        target: target,
      },
      port: port,
      host: host
    },
    module: {
      rules: [
        {
          test: /\.s?css$/,
          use: [
            mode !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader'
          ]
        }
      ]
    }, 
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "[name].css",
        chunkFilename: "[id].css"
      })
    ]
  }
}

