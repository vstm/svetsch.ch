---
title: Welcome
description: This website is about me, Stefan Vetsch.
layout: default
sitemap:
    lastmod: 2013-08-02 13:53:13 +0200
---

# Hi there #

My name is Stefan and I'm a software developer.

I don't know what to write, I just know it has to be funny yet smart. I'm
certain I will find something to write.

In the meantime: enjoy the lack of clever content.
