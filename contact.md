---
title: Contact/Social
description: How can you get in contact with Stefan Vetsch? This page will help
    you do that. I don't know why you would do that but hey, what do I know.
layout: default
sitemap:
    lastmod: 2018-11-21 09:25:00 +0100
---

# Contact #

You can write me at <a href="mailto:s@svetsch.ch">s@svetsch.ch</a> or
<a href="mailto:stefan.vetsch@gmail.com">stefan.vetsch@gmail.com</a>. I'm
happy to answer your mail.

## Social media stuff ##

Apparently even I can't escape that.

You find me on [Twitter @vetschs](https://twitter.com/vetschs). Well that's it
no Facebook no Instagram or whatever else is out there.

### Coding related ###

- [Stackexchange 
  ![Stackexchange Profile](https://stackexchange.com/users/flair/456660.png "Link to my Stackexchange profile")](https://stackexchange.com/users/456660)
- [GitHub](https://github.com/vstm)
- [Bitbucket](https://bitbucket.org/vstm/)

### Career related ###

- [Stackoverflow Careers 2.0](https://careers.stackoverflow.com/svetsch)
- [XING](https://www.xing.com/profile/Stefan_Vetsch3) (you need to have a XING-Account to view my profile)
- [LinkedIn](https://www.linkedin.com/in/stefan-vetsch-1a39a578/)
