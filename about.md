---
title: About
description: Here you learn about what I worked on, what I'm doing now and what
    some of my quirks are. I made this to scare people away from  me and my site.
layout: default
sitemap:
    lastmod: 2013-08-02 13:52:33 +0200
---

# About me #

I'm a software developer from the vicinity of Zürich, Switzerland, named - you
guessed it - Stefan Vetsch.

From 2001 to 2005 I did an apprenticeship as IT-whatever. Three of those four
years I was practically a C++ programmer, I was in a team which developed
software for the textile sector (quality control, dyehouse management). Then
from 2006 to 2009 I was re-hired as a full-time C++ programmer at the same
company. 

In 2009 I changed jobs (the previous company had to lay off a couple of people -
including yours truly). Since then I'm working for a small web-hosting company
near Zürich. I do a variety of things like customer support (E-Mail or phone),
Linux administration (even some Windows stuff) but mostly I do PHP-programming
(in-house and, on occasion, for customers).

I really like programming, I have tried out many programming languages - even
some lesser used ones like Erlang, Clojure or CLISP. I do a lot of learning and
experimenting in my free-time - just for the fun of it.

Currently I try to make more contributions to open-source projects.

I like:

 - Minimalism
 - To go for walks
 - To eat
 - To read books
 - To learn
 - Facts (they are very relaxing)
 - Parentheses (I don't follow the minimalistic-thing when I'm writing)

